const char debugger_fn_msg_01[] PROGMEM = "Port configuration init"; 
const char debugger_fn_msg_02[] PROGMEM = "WiFi Manager configuration init";
const char debugger_fn_msg_03[] PROGMEM = "AP SSID: ";
const char debugger_fn_msg_04[] PROGMEM = "AP PASS: ";
const char debugger_fn_msg_05[] PROGMEM = "Entering AP Mode";
const char debugger_fn_msg_06[] PROGMEM = "WiFi connected to network: ";
const char debugger_fn_msg_07[] PROGMEM = "With local IP: ";
const char debugger_fn_msg_08[] PROGMEM = "Setting web pages handlers";
const char debugger_fn_msg_09[] PROGMEM = "Page visited: ";
const char debugger_fn_msg_10[] PROGMEM = "api/ap-settings";
const char debugger_fn_msg_11[] PROGMEM = "settings/ap";
const char debugger_fn_msg_12[] PROGMEM = "Argument recieved: ";
const char debugger_fn_msg_13[] PROGMEM = "AP Params OK";
const char debugger_fn_msg_14[] PROGMEM = "AP Params ERROR";
const char debugger_fn_msg_15[] PROGMEM = "api/wf-settings";
const char debugger_fn_msg_16[] PROGMEM = "Saving WiFi credentials";
const char debugger_fn_msg_17[] PROGMEM = "WF Params OK";
const char debugger_fn_msg_18[] PROGMEM = "WF Params ERROR";
const char debugger_fn_msg_19[] PROGMEM = "wifi/reset";
const char debugger_fn_msg_20[] PROGMEM = "settings/wt";
const char debugger_fn_msg_21[] PROGMEM = "api/wt-settings";
const char debugger_fn_msg_22[] PROGMEM = "settings/time";
const char debugger_fn_msg_23[] PROGMEM = "api/time-settings";
const char debugger_fn_msg_24[] PROGMEM = "settings/tz";
const char debugger_fn_msg_25[] PROGMEM = "api/tz-settings";
const char debugger_fn_msg_26[] PROGMEM = "Initing Timers";


void configPorts(){
  debugger->debug(FPSTR(debugger_fn_msg_01));
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(HIGROMETER_PIN, INPUT);
  pinMode(FLOWMETER_PIN, INPUT_PULLUP);
  pinMode(ELECTROVALVE_PIN, OUTPUT);
  pinMode(WORKING_LED_PIN, OUTPUT);

  digitalWrite(ELECTROVALVE_PIN, LOW);
  digitalWrite(WORKING_LED_PIN, LOW);
}


void configWM(){
  debugger->debug(FPSTR(debugger_fn_msg_02));
  
  //When device connects to an AP
  WiFiMan.onConnect([]() {
    debugger->debug(FPSTR(debugger_fn_msg_06), WiFi.SSID());
    debugger->debug(FPSTR(debugger_fn_msg_07), WiFi.localIP().toString());

    //--> SAVING PARAMS
    if(!startupWifiConnect){
      debugger->debug(FPSTR(debugger_fn_msg_16));
      String new_ssid = WiFiMan.getWfSsid();
      String new_pass = WiFiMan.getWfPass();
      if(new_ssid.length() > 0 && (new_pass.length() == 0 || new_pass.length() > 7)){
        debugger->debug(FPSTR(debugger_fn_msg_17));
        dataWarehouse->set_wfssid(new_ssid);
        dataWarehouse->set_wfpass(new_pass);
      }
      else{
        debugger->debug(FPSTR(debugger_fn_msg_18));
      }
    }
    
    cloudSetup();
    
    
    //--> NET DISCOVERY
    EasySSDP::begin(server);
  });
  
  //When device goes to AP Mode
  WiFiMan.onAp([](){
    debugger->debug(FPSTR(debugger_fn_msg_05));
    debugger->debug(FPSTR(debugger_fn_msg_03),WiFiMan.getApSsid());
    debugger->debug(FPSTR(debugger_fn_msg_04),WiFiMan.getApPass());
  });

  //Access point configuration
  if(strlen(dataWarehouse->get_apssid()) > 0){
    if(strlen(dataWarehouse->get_appass()) > 7) WiFiMan.setApCredentials(dataWarehouse->get_apssid(), dataWarehouse->get_appass());
    else WiFiMan.setApCredentials(dataWarehouse->get_apssid());
  } else {
    if(strlen(dataWarehouse->get_appass()) > 7) WiFiMan.setApCredentials(deviceUID, dataWarehouse->get_appass());
    else WiFiMan.setApCredentials(deviceUID);
  }
  
  WiFiMan.setConnectNonBlock(true);         //Non Blocking Mode
  
}

void setWebHandlers(){
  debugger->debug(FPSTR(debugger_fn_msg_08));

  //--> DATA CONSULTS

  //--> SERVER.ON api/ap-settings --> Returns the JSON with AP parameters
  server.on("/api/ap-settings", []() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_10));
    
    //Build the JSON response
    StaticJsonDocument<200> jsonBuffer;
    JsonObject json = jsonBuffer.to<JsonObject>();
    
    if(WiFiMan.getApSsid().length() > 0) json["apssid"] = WiFiMan.getApSsid();
    else json["apssid"] = (char*)0;

    if(WiFiMan.getApPass().length() > 0) json["apssid"] = WiFiMan.getApPass();
    else json["appass"] = (char*)0;

    //Encode and response
    char jsonchar[200];
    serializeJson(jsonBuffer, jsonchar);
    server.send(200, "application/json", jsonchar);

  });

  //--> SERVER.ON api/wf-settings --> Returns the JSON with WiFi parameters
  server.on("/api/wf-settings", []() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_15));
    
    //Build the JSON response
    StaticJsonDocument<200> jsonBuffer;
    JsonObject json = jsonBuffer.to<JsonObject>();
    
    if(WiFiMan.getWfSsid().length() > 0) json["wfssid"] = WiFiMan.getWfSsid();
    else json["wfssid"] = (char*)0;

    if(WiFiMan.getWfPass().length() > 0) json["wfpass"] = WiFiMan.getWfPass();
    else json["wfpass"] = (char*)0;
    

    //Encode and response
    char jsonchar[200];
    serializeJson(jsonBuffer, jsonchar);
    server.send(200, "application/json", jsonchar);

  });

  //--> SERVER.ON api/wt-settings --> Returns the JSON with WiFi parameters
  server.on("/api/wt-settings", []() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_21));
    server.send(200, "application/json", dataWarehouse->get_wtconfig());
  });

  //--> SERVER.ON api/tz-settings --> Returns the JSON with TimeZone parameters
  server.on("/api/tz-settings", []() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_25));
    server.send(200, "application/json", dataWarehouse->get_tzconfig());
  });

  //--> SERVER.ON api/time-settings --> Returns the JSON with Time parameters
  server.on("/api/time-settings", []() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_23));
    RtcDateTime _clockTime = rtcClock->GetDateTime();

    //Build the JSON response
    StaticJsonDocument<200> _jsonBuffer;
    JsonObject _json = _jsonBuffer.to<JsonObject>();

    _json["YYYY"] = _clockTime.Year();
    _json["MM"] = _clockTime.Month();
    _json["DD"] = _clockTime.Day();
    _json["HH"] = _clockTime.Hour();
    _json["mm"] = _clockTime.Minute();
    _json["ss"] = _clockTime.Second();

    //Encode and response
    char jsonchar[200];
    serializeJson(_jsonBuffer, jsonchar);
    debugger->debug(jsonchar);
    
    server.send(200, "application/json", jsonchar);
  });

  //--> DATA SAVERS
  
  //--> SERVER.ON /settings/ap --> Saves the AP params
  server.on("/settings/ap", [&]() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_11));
    
    String new_ssid = server.arg("n");
    String new_pass = server.arg("p");

    debugger->debug(FPSTR(debugger_fn_msg_12), new_pass);
    debugger->debug(FPSTR(debugger_fn_msg_12), new_ssid);
  
    if(new_ssid.length() > 0 && (new_pass.length() == 0 || new_pass.length() > 7)){
      debugger->debug(FPSTR(debugger_fn_msg_13));
      server.send(200, "text/html", "RESTARTING");  //Make a new restart page
      dataWarehouse->set_apssid(new_ssid);
      dataWarehouse->set_appass(new_pass);
      
      delay(100);
      ESP.wdtDisable();
      ESP.reset();
      delay(2000);
    }
    else{
      debugger->debug(FPSTR(debugger_fn_msg_14));
      server.send(200, "text/html", "ERROR");  //Make a new restart page
    }
  
  });

  //--> SERVER.ON /wifi/reset --> Resets WF Params
  server.on("/wifi/reset", [&]() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_19));
    dataWarehouse->reset_wfconf();
    initServices();
  });

  //--> SERVER.ON /settings/wt --> Saves the watering params
  server.on("/settings/wt", [&]() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_20));

    //mtth=10&mttm=11&nsth=12&nstm=13&h=0&l=0&n=Lechuga&sd=1
    int mtth = server.arg("mtth").toInt();
    int mttm = server.arg("mttm").toInt();
    int nsth = server.arg("nsth").toInt();
    int nstm = server.arg("nstm").toInt();
    int _h = server.arg("h").toInt();
    float _hw = server.arg("l").toFloat();
    String plant_name = server.arg("n");
    int share = server.arg("sd").toInt();
    
    debugger->debug(FPSTR(debugger_fn_msg_12), String(mtth));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(mttm));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(nsth));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(nstm));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_h));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_hw));
    debugger->debug(FPSTR(debugger_fn_msg_12), plant_name);
    debugger->debug(FPSTR(debugger_fn_msg_12), String(share));

    if(mtth >= 0 && mtth <= 24 && nsth >= 0 && nsth <= 24 && mttm >= 0 && mttm <= 60 && nstm >= 0 && nstm <= 60 && _h >= 0 && _h <= 100 && _hw >= 0 && plant_name.length() > 0 && plant_name.length() <= 100){
      //Build the JSON response
      StaticJsonDocument<400> _jsonBuffer;
      JsonObject _json = _jsonBuffer.to<JsonObject>();
      plant_name.setCharAt(0, toupper(plant_name.charAt(0)));

      _json["mtth"] = mtth;
      _json["mttm"] = mttm;
      _json["nsth"] = nsth;
      _json["nstm"] = nstm;
      _json["h"] = _h;
      _json["l"] = _hw;
      _json["sd"] = share;
      _json["n"] = plant_name;

      //Encode and response
      char jsonchar[400];
      serializeJson(_jsonBuffer, jsonchar);
      debugger->debug(jsonchar);
      dataWarehouse->set_wtconfig(jsonchar);
      
      server.send(200, "application/json", "{\"saved\": true}");
    }
    else server.send(200, "application/json", "{\"saved\": false}");
  });

//--> SERVER.ON /settings/time --> Saves the time params
  server.on("/settings/time", [&]() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_22));

    //yy=2018&mm=11&dd=4&hh=12&mi=21&ss=21
    int _year = server.arg("yy").toInt();
    int _month = server.arg("mm").toInt();
    int _day = server.arg("dd").toInt();
    int _hours = server.arg("hh").toInt();
    int _minutes = server.arg("mi").toInt();
    int _seconds = server.arg("ss").toInt();
    
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_year));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_month));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_day));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_hours));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_minutes));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(_seconds));

    if(_year >= 0 && _month <= 12 && _month >= 1  && _day >= 1 && _day <= 31 && _hours >= 0 && _hours <= 60 && _minutes >= 0 && _minutes <= 60 && _seconds >= 0 && _seconds <= 60){

      RtcDateTime new_date = RtcDateTime(_year, _month, _day, _hours, _minutes, _seconds);
      rtcClock->SetDateTime(new_date);
      server.send(200, "application/json", "{\"saved\": true}");
      
    }
    else server.send(200, "application/json", "{\"saved\": false}");
  });

  //--> SERVER.ON /settings/tz --> Saves the timezone params
  server.on("/settings/tz", [&]() {
    debugger->debug(FPSTR(debugger_fn_msg_09),FPSTR(debugger_fn_msg_24));

    //country=Barbados&region=Saint Michael&utc_ooffset=-6&ntp_sync=1
    String country = server.arg("country");
    String region = server.arg("region");
    float utc_offset = server.arg("utc_offset").toFloat();
    int antp = server.arg("ntp_sync").toInt();
    
    debugger->debug(FPSTR(debugger_fn_msg_12), country);
    debugger->debug(FPSTR(debugger_fn_msg_12), region);
    debugger->debug(FPSTR(debugger_fn_msg_12), String(utc_offset));
    debugger->debug(FPSTR(debugger_fn_msg_12), String(antp));

    if(antp >= 0 && antp <= 1 && utc_offset >= -14 && utc_offset <= 14){
      
      //Build the JSON response
      StaticJsonDocument<400> _jsonBuffer;
      JsonObject _json = _jsonBuffer.to<JsonObject>();
      country.setCharAt(0, toupper(country.charAt(0)));
      region.setCharAt(0, toupper(region.charAt(0)));

      _json["country"] = country;
      _json["region"] = region;
      _json["utc_offset"] = utc_offset;
      _json["antp"] = antp;

      //Encode and response
      char jsonchar[400];
      serializeJson(_jsonBuffer, jsonchar);
      debugger->debug(jsonchar);
      dataWarehouse->set_tzconfig(jsonchar);
      
      server.send(200, "application/json", "{\"saved\": true}");
    }
    else server.send(200, "application/json", "{\"saved\": false}");
  });
  
}

void initServices(){
  setWebHandlers();
  startupWifiConnect = true;                  //Not rewriting flag
  WiFiMan.begin(dataWarehouse->get_wfssid(), dataWarehouse->get_wfpass());                          //Starts the WiFi Manager
  startupWifiConnect = false;                 //No rewriting flag
  server.begin();                             //HTTP Server start
}

void configureTimers(){
  debugger->debug(FPSTR(debugger_fn_msg_26));
  _dataProcessTimer.attach(_dataInterval, processData);
}
