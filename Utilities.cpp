#include <Arduino.h>
#include <SPIFFSReadServer.h>
#include <ArduinoJson.h>
#include <Wire.h>
#include <RtcDS3231.h>

const char debugger_01[] PROGMEM = "Debugger.DEBUG: "; 
const char debugger_02[] PROGMEM = "Debugger.ERROR: ";
const char sets_str_01[] PROGMEM = "File not found: ";
const char sets_str_02[] PROGMEM = "Starting DM";
const char sets_str_03[] PROGMEM = "File invalid: ";
const char sets_str_04[] PROGMEM = "File empty: ";
const char sets_str_05[] PROGMEM = "Length invalid: ";
const char sets_str_06[] PROGMEM = "File read OK: ";
const char sets_str_07[] PROGMEM = "File found, rewriting: ";
const char sets_str_08[] PROGMEM = "File not found, creating: ";
const char sets_str_09[] PROGMEM = "Ready to write: ";
const char sets_str_10[] PROGMEM = "Ready to read: ";
const char sets_str_11[] PROGMEM = "File write OK: ";
const char sets_str_12[] PROGMEM = "File write FAIL: ";
const char sets_str_13[] PROGMEM = "Expected value: ";
const char sets_str_14[] PROGMEM = "Read value: ";
const char sets_str_15[] PROGMEM = "JSON ERROR: ";

const char sets_appass[] PROGMEM = "/appass";
const char sets_apssid[] PROGMEM = "/apssid";
const char sets_wfpass[] PROGMEM = "/wfpass";
const char sets_wfssid[] PROGMEM = "/wfssid";
const char sets_wtconf[] PROGMEM = "/wtconf";
const char sets_tzconf[] PROGMEM = "/tzconf";

namespace Utilities{
  
  class Debugger{

    bool _isActive;
    
    public:
    
      Debugger(bool isActive){
        this->_isActive = isActive;
        if(!Serial){
          Serial.begin(115200);
        }
      }

      void setActive(bool isActive){
        this->_isActive = isActive;
      }

      bool isActive(){
        return this->_isActive;
      }

      void debug(String message){
        if(_isActive){
          Serial.print(FPSTR(debugger_01));
          Serial.println(message);
        }
      }
      
      void debug(String header, String message){
        if(_isActive){
          Serial.print(FPSTR(debugger_01));
          Serial.print(header);
          Serial.println(message);
        }
      }

      void error(String message){
        Serial.print(FPSTR(debugger_02));
        Serial.println(message);
      }

      void error(String header, String message){
        Serial.print(FPSTR(debugger_02));
        Serial.print(header);
        Serial.println(message);
      }
    
  };

  class DataManager{

    //--> NETWORK GLOBAL DATA
    bool _settingsExists;
    bool _dataExists;
    char _apssid[33];
    char _appass[33];
    char _wfssid[33];
    char _wfpass[33];
    char _wtconf[400];
    char _tzconf[400];

    //--> WATERING GLOBAL DATA
    bool _waterDataExists;
    int _mtth;
    int _mttm;
    int _nsth;
    int _nstm;
    bool _sharing;
    char _plant_name[101];
    int _humidity;
    float _water_lt;
    
    //--> TIME GLOBAL DATA
    bool _timezoneDataExists;
    char _device_country[101];
    char _device_region[101];
    bool _allow_ntp;
    float _utc_offset;

    //--> STATISTICS
    
    Debugger *_debugger;

    void read_data(char *varname, String filename){
      File f;
      char *varname_bu = varname;
      if(SPIFFS.exists(filename)){
        f = SPIFFS.open(filename, "r");
        if (f && f.size() > 0 && f.size() < 400) {
          _debugger->debug(FPSTR(sets_str_06), filename);
         while (f.available()){
            *varname = char(f.read());
            varname++;
          }
        }
        else if(f && f.size() < 1) {
          _debugger->error(FPSTR(sets_str_04), filename);
        }
        else if(f && f.size() > 33) {
          _debugger->error(FPSTR(sets_str_05), filename);
        }
        else if(!f){
          _debugger->error(FPSTR(sets_str_03), filename);
        }
        f.close();
      }
      else {
        _debugger->error(FPSTR(sets_str_01), filename);
      }
      *varname = '\0';
      _debugger->debug(FPSTR(sets_str_14), varname_bu);  
    }

    bool write_data(String newVal, String filename){
      File f;
      if(SPIFFS.exists(filename)){
        _debugger->debug(FPSTR(sets_str_07), filename);       //File exists
      }
      else{
        _debugger->debug(FPSTR(sets_str_08), filename);      //File not exists
      }
      
      f = SPIFFS.open(filename, "w");
      
      if(f){
        _debugger->debug(FPSTR(sets_str_09), filename);       //Open file
        if(f.print(newVal)) {
          _debugger->debug(FPSTR(sets_str_11), filename);     //File writed ok
          return true;
        }
        else{
          if(newVal.length() == 0){
            _debugger->debug(FPSTR(sets_str_11), filename);     //File writed ok
            return true;
          }
          else{
            return false;
            _debugger->error(FPSTR(sets_str_12), filename);     //Failed to write
          }
        }
      }
      else{
        _debugger->error(FPSTR(sets_str_03), filename);       //Could not open file
        return false;
      }
    }

    bool setWateringData(){
      StaticJsonDocument<400> doc;
      auto err = deserializeJson(doc, _wtconf, 400);
      JsonObject root = doc.as<JsonObject>();
      if(err){
        _debugger->error(FPSTR(sets_str_15), err.c_str());
        _waterDataExists = false;
        return _waterDataExists;
      }
      
      _waterDataExists = true;
      _mtth = root["mtth"];
      _mttm = root["mttm"];
      _nsth = root["nsth"];
      _nstm = root["nstm"];
      _sharing = root["sd"];
      root["n"].as<String>().toCharArray(_plant_name, 99);
      _humidity = root["h"];
      _water_lt = root["l"];

      return _waterDataExists;
    }

    bool setTimezoneData(){
      DynamicJsonDocument doc(400);
      auto err = deserializeJson(doc, _tzconf, 400);
      
      JsonObject root = doc.as<JsonObject>();
      if(err){
        _debugger->error(FPSTR(sets_str_15), err.c_str());
        _timezoneDataExists = false;
        return _timezoneDataExists;
      }
      
      _timezoneDataExists = true;
      root["region"].as<String>().toCharArray(_device_region, 100);
      root["country"].as<String>().toCharArray(_device_country, 100);
      _allow_ntp = root["antp"];
      _utc_offset = root["utc_offset"];

      return _timezoneDataExists;
    }

    public:
      DataManager(Debugger& debug){
        SPIFFS.begin();
        _debugger = &debug;
        _debugger->debug(FPSTR(sets_str_02));
        read_data(&_apssid[0], FPSTR(sets_apssid));
        read_data(&_appass[0], FPSTR(sets_appass));
        read_data(&_wfssid[0], FPSTR(sets_wfssid));
        read_data(&_wfpass[0], FPSTR(sets_wfpass));
        read_data(&_wtconf[0], FPSTR(sets_wtconf));
        setWateringData();
        read_data(&_wtconf[0], FPSTR(sets_wtconf));
        read_data(&_tzconf[0], FPSTR(sets_tzconf));
        setTimezoneData();
        read_data(&_tzconf[0], FPSTR(sets_tzconf));
      }

      //--> GETTERS
      char* get_apssid(){ return &_apssid[0]; }
      char* get_appass(){ return &_appass[0]; }
      char* get_wfssid(){ return &_wfssid[0]; }
      char* get_wfpass(){ return &_wfpass[0]; }
      char* get_wtconfig(){ return &_wtconf[0]; }
      char* get_tzconfig(){ return &_tzconf[0]; }
      
      float get_water_qty(){ return _water_lt; }
      int get_morning_till_hour(){ return _mtth; }
      int get_morning_till_minute(){ return _mttm; }
      int get_nigth_since_hour(){ return _nsth; }
      int get_nigth_since_minute(){ return _nstm; }
      int get_humidity_th(){ return _humidity; }

      /*  NO GETTERS FOR
            //--> WATERING GLOBAL DATA
            bool _waterDataExists;
            bool _sharing;
            char _plant_name[101];
            int _humidity;
            
            //--> TIME GLOBAL DATA
            bool _timezoneDataExists;
            char _device_country[101];
            char _device_region[101];
            bool _allow_ntp;
            float _utc_offset;
       */

      //--> SETTERS
      bool set_apssid(String newVal){
        if(write_data(newVal, FPSTR(sets_apssid))){
          read_data(&_apssid[0], FPSTR(sets_apssid));
          _debugger->debug(FPSTR(sets_str_13), newVal);
          return (strcmp(_apssid,newVal.c_str())==0);
        }
        else return false;
      }
      
      bool set_appass(String newVal){
        if(newVal.length() == 0 || newVal.length() > 7){
          if(write_data(newVal, FPSTR(sets_appass))){
            read_data(&_appass[0], FPSTR(sets_appass));
            _debugger->debug(FPSTR(sets_str_13), newVal);
            return (strcmp(_appass,newVal.c_str())==0);
          }
          else return false;
        }
        else  return false;
      }

      
      bool set_wfssid(String newVal){
        if(write_data(newVal, FPSTR(sets_wfssid))){
          read_data(&_wfssid[0], FPSTR(sets_wfssid));
          _debugger->debug(FPSTR(sets_str_13), newVal);
          return (strcmp(_wfssid,newVal.c_str())==0);
        }
        else return false;
      }

      
      bool set_wfpass(String newVal){
        if(newVal.length() == 0 || newVal.length() > 7){
          if(write_data(newVal, FPSTR(sets_wfpass))){
            read_data(&_wfpass[0], FPSTR(sets_wfpass));
            _debugger->debug(FPSTR(sets_str_13), newVal);
            return (strcmp(_wfpass,newVal.c_str())==0);
          }
          else return false;
        }
        else  return false;
      }

      bool set_wtconfig(String newVal){
        if(write_data(newVal, FPSTR(sets_wtconf))){
          read_data(&_wtconf[0], FPSTR(sets_wtconf));
          setWateringData();
          read_data(&_wtconf[0], FPSTR(sets_wtconf));
          _debugger->debug(FPSTR(sets_str_13), newVal);
          return (strcmp(_wtconf,newVal.c_str())==0);
        }
        else return false;
      }

      bool set_tzconfig(String newVal){
        if(write_data(newVal, FPSTR(sets_tzconf))){
          read_data(&_tzconf[0], FPSTR(sets_tzconf));
          setTimezoneData();
          read_data(&_tzconf[0], FPSTR(sets_tzconf));
          _debugger->debug(FPSTR(sets_str_13), newVal);
          return (strcmp(_tzconf,newVal.c_str())==0);
        }
        else return false;
      }

      //-->RESETERS
      bool reset_apssid(){ return set_apssid(""); }
      bool reset_appass(){ return set_appass(""); }
      bool reset_wfssid(){ return set_wfssid(""); }
      bool reset_wfpass(){ return set_wfpass(""); }
      
      bool reset_wtconfig(){ return set_wtconfig(""); }
      bool reset_tzconfig(){ return set_tzconfig(""); }
      bool reset_apconf(){ return (set_apssid("") && set_appass("")); }
      bool reset_wfconf(){ return (set_wfssid("") && set_wfpass("")); }
      
    
  };
  
}
