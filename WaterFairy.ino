#include <ESP8266WiFi.h>                    //Board library
#include <DzWiFiManager.h>                  //WiFiManager Fork
#include <ArduinoJson.h>                    //Json for arduino >@6.0.0
#include <EasySSDP.h>                       //Discovery network service
#include <SPIFFSReadServer.h>               //SPI Flash FileSystem Server
#include <Ticker.h>                         //Software based timers
#include <DNSServer.h>                      //DNS functions for AP mode
#include <FS.h>                             //FileSystem utilities
#include <PubSubClient.h>                   //MTQQ Client for Publish and Subscribe

//--> PROJECT MODULES
#include "Utilities.cpp"                    //Utilidades del proyecto

//--> DEFINES
#define DEVICE_NAME "Water Fairy"
#define BUTTON_PIN D3
#define HIGROMETER_PIN A0
#define SDA_PIN D2
#define SCL_PIN D1
#define ELECTROVALVE_PIN D0
#define FLOWMETER_PIN D4
#define WORKING_LED_PIN D5
#define WORKING_LED_INTERVAL .5             //SECONDS

//--> GLOBAL VARS
SPIFFSReadServer server(80);                //WebServer with SPIFFS
DNSServer dnsServer;                        //DNS server
DzWiFiManager WiFiMan(server, dnsServer);   //WifiManager 
Utilities::Debugger *debugger;              //Global debugger
Utilities::DataManager *dataWarehouse;      //Data warehouse
RtcDS3231<TwoWire> *rtcClock;               //Clock Object

//--> GLOBAL DATA
char deviceUID[22];                         //Stores device + UID
char uID[9];                                //Stores device UID (MAC Identity)
bool startupWifiConnect;                    //Flag for first WiFi connection
float _dataInterval = 60;                   //Data reporting interval in seconds
float _WiFiReconnectInterval = 60;          //Retry interval for WiFi in seconds **
Ticker _WiFiReconnectTimer;                 //Retries connection if drop **
Ticker _dataProcessTimer;                   //Process the data incoming
double lastSecondFlow;                      //Flow of the last second (in liters per minute)
bool isWateringActive = false;              //Is the device watering

//--> PROGRAM FUNCTIONS
void configPorts();                                 //Configure GPIO Ports
void configWM();                                    //Configures Wireless Manager
void initServices();                                //Initializes services
void setWebHandlers();                              //Sets the handlers for custom pages
void buttonHandler();                               //Button handler
void ICACHE_RAM_ATTR handleButtonInterrupt();       //ISR for button
void buttonClean();                                 //Cleans button presses
void buttonFunctions();                             //Executes button commands
void configureTimers();                             //Sets all the timers
void ICACHE_RAM_ATTR processData();                 //Process the data (Stores and report)
void ICACHE_RAM_ATTR handleFlowmeterInterrupt();    //Handles the flow meter interrupt
void beginWatering();                               //Starts the watering process
void stopWatering();                                //Stops the watering process
void ICACHE_RAM_ATTR blinkWorkingLed();             //Turns ON/OFF the working led

void remoteManaging();                              //Mantains cloud connections (MUST BE CALLED EVERY MAIN LOOP!!)
void cloudSetup();                                  //Begin setup Configurations

//--> GP FLASH STRINGS
const char debugger_gp_01[] PROGMEM = "Setup Complete";

void setup(){
  snprintf(uID,9,"%08X",ESP.getChipId());                                     //FORMS CHIP UID
  snprintf(deviceUID,22,"Water Fairy-%08X",ESP.getChipId());                  //FORMS DEVICE-CHIP UID
  
  debugger = new Utilities::Debugger(true);                                   //Debugger init
  rtcClock = new RtcDS3231<TwoWire>(Wire);                                    //Clock Init
  rtcClock->Begin();                                                          //Clock service begin
  configPorts();                                                              //Ports configuration
  SPIFFS.begin();                                                             //Flash Filesystem begin
  dataWarehouse = new Utilities::DataManager(*debugger);                      //Data container
  
  //SPIFFS.format();                                        ยบ                 //Cleans filesystem
  //dataWarehouse->set_apssid("ProbandoDatosPermanentes");
  //dataWarehouse->reset_apconf();

  configWM();
  initServices();
  //cloudSetup();
  
  attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), handleButtonInterrupt, FALLING);
  
  configureTimers();
  debugger->debug(FPSTR(debugger_gp_01));
  
}

void loop(){
  WiFiMan.handleWiFi();
  dnsServer.processNextRequest();
  server.handleClient();
  remoteManaging();
  //Serial.println(_dataProcessTimer.active());
  
  //debugger.debug("Hola");
  //Serial.println(deviceUID);
  //debugger.error("Adios");
  //delay(1000);
}
