const char watering_debug_msg_01[] PROGMEM = "Flow tick ISR. Counter: ";
const char watering_debug_msg_02[] PROGMEM = "Flow limit reached, stop watering";
const char watering_debug_msg_03[] PROGMEM = "Last second flow: ";
const char watering_debug_msg_04[] PROGMEM = "Gather Soil Humidity: ";
const char watering_debug_msg_05[] PROGMEM = "Gathered flow ticks: ";
const char watering_debug_msg_06[] PROGMEM = "Process Data Cycle";
const char watering_debug_msg_07[] PROGMEM = "Watering Allowed: ";
const char watering_debug_msg_08[] PROGMEM = "Watering begin";
const char watering_debug_msg_09[] PROGMEM = "Watering stop";
const char watering_debug_msg_10[] PROGMEM = "Flow tick limit: ";
const char watering_debug_msg_11[] PROGMEM = "MQTT connection established";
const char watering_debug_msg_12[] PROGMEM = "Subscribing to: ";
const char watering_debug_msg_13[] PROGMEM = "Publishing to: ";
const char watering_debug_msg_14[] PROGMEM = "Setting up MQTT";
const char watering_debug_msg_15[] PROGMEM = "MQTT Failed Connection";
const char watering_debug_msg_16[] PROGMEM = "MQTT is active, reporting data";
const char watering_debug_msg_17[] PROGMEM = "MQTT has failed, skipped reports: ";
const char watering_debug_msg_18[] PROGMEM = "MQTT subscribe callback, topic: ";
const char watering_debug_msg_19[] PROGMEM = "MQTT failed to parse callback, topic: ";
const char watering_debug_msg_20[] PROGMEM = "REMOTE: ";


#define ML_PER_TICK 2.05
#define HIGROMETER_VCC_INPUT 5
#define SATURATED_SOIL_LEVEL 340
#define DRY_SOIL_LEVEL 930

//--> TELEMETRY
#define BBT "mqtt.beebotte.com"
#define TOKEN "token:token_Q7LhSjrLKQSbwHET"
#define CHANNEL "WaterFairy"                //Channel Name
#define TOTALWATER_RESOURCE "TotalWater"    //Resource Name
#define HUMIDITY_RESOURCE "Humidity"        //Resource Name
#define ISWATERING_RESOURCE "IsWatering"    //Resource Name
#define REMOTESTART_RESOURCE "RemoteStart"  //Resource Name
#define PERIODFLOW_RESOURCE "PeriodFlow"    //Resource Name

const char chars[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
char id[17];

long currentFlowTicks = 0;                //This cycle flow ticks
long lastCheckedFlow = 0;                 //Flow ticks when water flow cheked
long pendingFlowTicks = 0;                //Ticks needed to report
bool hasFailedReport = false;             //Last report has failed
int skippedReports = 0;                   //How many times last report has failed
Ticker waterFlowTimer;                    //Checks the water is flowing throug the pipe
Ticker workingLedTimer;                   //Timer of the working led
float noWaterFlowInterval = 1;            //noWaterFlowTimer interval (in seconds)
bool isWaterFlowing = false;              //Flag for water flow
long flowTicksLimit = 0;                  //Required water flow in ticks
bool invalidFlowCycle = false;            //Flags an invalid flow cycle
double currentHigro = 0;                  //Stores current higro percentage (0.00 to 100.0)
bool isWateringAllowed = false;           //Flag if time is between watering times
int waterFlowErrors;                      //How many secuenced times has failed the water flow measuring

//Water and telemetry functions
void reportData();                        //Reports data to local [and cloud if active]
void gatherData();                        //Recolect data from sensors
void wateringActions();                   //Takes watering decision based on data from sensors
void ICACHE_RAM_ATTR checkWaterFlow();    //Checks current flow

void recievedMessage(char* topic, byte* payload, unsigned int length);
Ticker mqttReconnectTimer;
void retryMqttConnect();
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);
bool mqttPublish(const char* resource, float data, bool persist);

void wateringActions(){
  if(!isWateringActive){
    if(currentHigro <= dataWarehouse->get_humidity_th()){ //Water need
      if(isWateringAllowed){
        beginWatering();
      }
      else{
        //ALERT OF SOIL DRY
      }
    }
  }
}

void processData(){
  debugger->debug(FPSTR(watering_debug_msg_06));
  gatherData();
  wateringActions();
  reportData();
}

void gatherData(){
  //Flowmeter Data
  if(isWateringActive) detachInterrupt(digitalPinToInterrupt(FLOWMETER_PIN));
  pendingFlowTicks += currentFlowTicks;
  currentFlowTicks = 0;
  lastCheckedFlow = 0;
  invalidFlowCycle = true;
  if(isWateringActive) attachInterrupt(digitalPinToInterrupt(FLOWMETER_PIN), handleFlowmeterInterrupt, FALLING);

  //Higrometer Data
  double higroLevel = analogRead(A0) - SATURATED_SOIL_LEVEL;
  if(higroLevel > DRY_SOIL_LEVEL) higroLevel = DRY_SOIL_LEVEL;
  if(higroLevel < 0) higroLevel = 0;
  if(higroLevel > (DRY_SOIL_LEVEL - SATURATED_SOIL_LEVEL)) higroLevel = DRY_SOIL_LEVEL - SATURATED_SOIL_LEVEL;
  currentHigro = 100 - (higroLevel /((DRY_SOIL_LEVEL - SATURATED_SOIL_LEVEL)/100.00));

  //Watering Time
  RtcDateTime _clockTime = rtcClock->GetDateTime();
  if(_clockTime.Hour() <= dataWarehouse->get_morning_till_hour() || _clockTime.Hour() >= dataWarehouse->get_nigth_since_hour()){
    if(_clockTime.Hour() == dataWarehouse->get_morning_till_hour() || _clockTime.Hour() == dataWarehouse->get_nigth_since_hour()){
      if(_clockTime.Hour() == dataWarehouse->get_morning_till_hour() && _clockTime.Minute() <= dataWarehouse->get_morning_till_minute()){
        isWateringAllowed = true;
      } else if(_clockTime.Hour() >= dataWarehouse->get_nigth_since_hour() && _clockTime.Minute() >= dataWarehouse->get_nigth_since_minute()){
        isWateringAllowed = true;
      }
     else{
        isWateringAllowed = false;
      }
    }
    else{
      isWateringAllowed = true;
    }
  }
  else{
    isWateringAllowed = false;
  }

  debugger->debug(FPSTR(watering_debug_msg_05), String(pendingFlowTicks));
  debugger->debug(FPSTR(watering_debug_msg_04), String(higroLevel));
  debugger->debug(FPSTR(watering_debug_msg_04), String(currentHigro));
  debugger->debug(FPSTR(watering_debug_msg_07), String(isWateringAllowed));
}

void handleFlowmeterInterrupt(){
  currentFlowTicks++;
  debugger->debug(FPSTR(watering_debug_msg_01), String(currentFlowTicks));
  if(currentFlowTicks >= flowTicksLimit){
    debugger->debug(FPSTR(watering_debug_msg_02));
    stopWatering();
  }
}

void beginWatering(){
  if(!isWateringActive){
    debugger->debug(FPSTR(watering_debug_msg_08));
    isWateringActive = true;
    flowTicksLimit = ceil((dataWarehouse->get_water_qty() * 1000)/ ML_PER_TICK);
    debugger->debug(FPSTR(watering_debug_msg_10), String(flowTicksLimit));
    waterFlowTimer.attach(noWaterFlowInterval, checkWaterFlow);
    attachInterrupt(digitalPinToInterrupt(FLOWMETER_PIN), handleFlowmeterInterrupt, FALLING);
    lastCheckedFlow = 0;
    
    //OPENING ELECTROVALVE
    digitalWrite(ELECTROVALVE_PIN, HIGH);
    
    //WORKING PIN LED
    workingLedTimer.attach(WORKING_LED_INTERVAL, blinkWorkingLed);
  }
}

void stopWatering(){
  debugger->debug(FPSTR(watering_debug_msg_09));
  waterFlowTimer.detach();
  isWateringActive = false;
  lastCheckedFlow = 0;
  workingLedTimer.detach();
  digitalWrite(ELECTROVALVE_PIN, LOW);
  detachInterrupt(digitalPinToInterrupt(FLOWMETER_PIN));
  if(isWorkingLedOn) blinkWorkingLed();
}

void checkWaterFlow(){
  if(!invalidFlowCycle){
    if(currentFlowTicks <= lastCheckedFlow){
      isWaterFlowing = false;
      waterFlowErrors++;
      if(waterFlowErrors > 10){
        waterFlowErrors = 0;
        Serial.println("No hay flujo");
        stopWatering();
      }
    }
    else{
      isWaterFlowing = true;
      lastSecondFlow = (((currentFlowTicks - lastCheckedFlow) * 60) * ML_PER_TICK)/1000;
      debugger->debug(FPSTR(watering_debug_msg_03), String(lastSecondFlow));
      lastCheckedFlow = currentFlowTicks;
      waterFlowErrors = 0;
    }
  }
  else invalidFlowCycle = false;
}

void reportData(){
  if(WiFi.status() == WL_CONNECTED && mqttClient.connected()){
    debugger->debug(FPSTR(watering_debug_msg_16));
    hasFailedReport = false;
    skippedReports = 0;

    mqttPublish(HUMIDITY_RESOURCE, currentHigro, true);             //HUMIDITY
    mqttPublish(REMOTESTART_RESOURCE, isWateringActive, true);      //REMOTESTART_RESOURCE
    //#define TOTALWATER_RESOURCE "TotalWater"    //Resource Name
    //#define PERIODFLOW_RESOURCE "PeriodFlow"    //Resource Name
  }
  else {
    hasFailedReport = true;
    skippedReports++;
    debugger->debug(FPSTR(watering_debug_msg_17), String(skippedReports));
  }
}

bool mqttPublish(const char* resource, float data, bool persist){
    StaticJsonDocument<200> jsonOutBuffer;
    JsonObject root = jsonOutBuffer.to<JsonObject>();
    root["channel"] = CHANNEL;
    root["resource"] = resource;
    root["write"] = persist;
    
    root["data"] = data;

    char buffer[200];
    serializeJson(jsonOutBuffer, buffer);

    // Create the topic to publish to
    char topic[64];
    sprintf(topic, "%s/%s", CHANNEL, resource);
    debugger->debug(FPSTR(watering_debug_msg_13), topic);

    return mqttClient.publish(topic, buffer);
}

const char * generateID(){
  randomSeed(analogRead(0));
  int i = 0;
  for(i = 0; i < sizeof(id) - 1; i++) {
    id[i] = chars[random(sizeof(chars))];
  }
  id[sizeof(id) -1] = '\0';

  return id;
}

void recievedMessage(char* topic, byte* payload, unsigned int length) {

  StaticJsonDocument<200> jsonInBuffer;
  auto err = deserializeJson(jsonInBuffer, payload);
  JsonObject root = jsonInBuffer.as<JsonObject>();

  if (err) {
    debugger->debug(FPSTR(watering_debug_msg_19), topic);
    return;
  }

  debugger->debug(FPSTR(watering_debug_msg_18), topic);  

    char remotestart_str[64];
    sprintf(remotestart_str, "%s/%s", CHANNEL, REMOTESTART_RESOURCE);
    Serial.println(strcmp(topic, remotestart_str));
  if(strcmp(topic, remotestart_str) == 0){
    bool remoteStart = root["data"];
    if(remoteStart && !isWateringActive){
      debugger->debug(FPSTR(watering_debug_msg_20), FPSTR(watering_debug_msg_08));
      beginWatering();
    }
    else if(!remoteStart && isWateringActive){
      debugger->debug(FPSTR(watering_debug_msg_20), FPSTR(watering_debug_msg_09));
      stopWatering();
    }
  }
}

void remoteManaging(){
  if (!mqttClient.connected()) {
    if(!mqttReconnectTimer.active()){
      mqttReconnectTimer.attach(5,  tryMqttConnect);
    }
  }
  else mqttClient.loop();
}

void tryMqttConnect(){
  if (mqttClient.connect(generateID(), TOKEN, "")) {
    char topic[64];
    sprintf(topic, "%s/%s", CHANNEL, REMOTESTART_RESOURCE);
    debugger->debug(FPSTR(watering_debug_msg_12), topic);
    mqttClient.subscribe(topic);
    debugger->debug(FPSTR(watering_debug_msg_11));
    if(mqttReconnectTimer.active()) mqttReconnectTimer.detach();
  }
  else debugger->debug(FPSTR(watering_debug_msg_15));
}

void cloudSetup(){
  debugger->debug(FPSTR(watering_debug_msg_14));
  mqttClient.setServer(BBT, 1883);
  mqttClient.setCallback(recievedMessage);
  tryMqttConnect();
}
