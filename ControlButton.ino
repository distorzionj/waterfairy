#define LONG_PRESS_TIME 20      //.1 seconds each
Ticker buttonDebouncer;
Ticker buttonCleaner;
int _shortPress = 0;
int _longCounter = 0;
bool isWorkingLedOn = false;
const char button_debug_msg_01[] PROGMEM = "Short press: ";
const char button_debug_msg_02[] PROGMEM = "Execute command";
const char button_debug_msg_03[] PROGMEM = "Inited ISR";
const char button_debug_msg_04[] PROGMEM = "Blinking Led";


void buttonHandler(){
  if(digitalRead(BUTTON_PIN) == 0){// STILL PRESSED
    _longCounter++;
    if(_longCounter >= LONG_PRESS_TIME){ //PRESSED FOR 2 SECONDS
      buttonFunctions();
    }
    else{
      buttonDebouncer.once(0.1, buttonHandler);
    }
  }
  else{
    attachInterrupt(digitalPinToInterrupt(BUTTON_PIN), handleButtonInterrupt, FALLING);  
  }
  buttonCleaner.detach();
  buttonCleaner.once(1,buttonClean);
}

void buttonClean(){
  _shortPress = 0;
  _longCounter = 0;
}

void handleButtonInterrupt(){
  debugger->debug(FPSTR(button_debug_msg_03));
  detachInterrupt(digitalPinToInterrupt(BUTTON_PIN));
  _shortPress++;
  debugger->debug(FPSTR(button_debug_msg_01), String(_shortPress));
  buttonDebouncer.once(0.1, buttonHandler);
}

void buttonFunctions(){
  debugger->debug(FPSTR(button_debug_msg_02));
  switch(_shortPress){
    case 1:
      if(!isWateringActive) beginWatering();
    break;
    case 2:
      if(isWateringActive) stopWatering();
    break;
  }
  buttonDebouncer.once(5, buttonHandler);
  buttonClean();
}

void blinkWorkingLed(){
  if(isWorkingLedOn){
    digitalWrite(WORKING_LED_PIN, LOW);
    isWorkingLedOn = false;
  }
  else{
    digitalWrite(WORKING_LED_PIN, HIGH);
    isWorkingLedOn = true;
  }
  //debugger->debug(FPSTR(button_debug_msg_04));
}
